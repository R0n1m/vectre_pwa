import Vue from "vue";
import "spectre.css/dist/spectre-exp.css";
import "spectre.css/dist/spectre-icons.css";
import "spectre.css/dist/spectre.css";
import { VectrePlugin } from "@vectrejs/vectre";
import "captain-icons/dist/captain-icons.css";
import router from "./router";
import App from "./App.vue";
import "./registerServiceWorker";

Vue.config.productionTip = false;

Vue.use(VectrePlugin);
new Vue({
    router,
    render: (h) => h(App),
}).$mount("#app");